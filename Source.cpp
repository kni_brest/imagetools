#pragma once
#include "Header.h";

using namespace std;
using namespace Magick;

#pragma comment( lib, "CORE_RL_Magick++_" )	

// �-�, ������������� � ������������ ������ � ���� �����
void mesconsol(string s, int pr);
COORD GetConsoleCursorPosition(HANDLE hConsoleOutput);
void markAsDone(string str);
string GetStrHelp(string& forhelp);

vector<comElement> comTabl;


void main() {

	setlocale(LC_CTYPE, "rus"); // ����� ������� ��������� ������

	// Current Dir
	char current_work_dir[FILENAME_MAX];
	_getcwd(current_work_dir, sizeof(current_work_dir));
	string str = current_work_dir;

	int pos = str.rfind("\App");
	string dop, forhelp = "";
	if (pos == -1) //Run from VS 
	{
		   dop = "App\\Images\\";
		   forhelp == str;
		   forhelp = str + "\\App\\" ;
	}
	else
	{
		dop = "Images\\";
		forhelp = "";
	}

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	Init(); // ������������� Magick
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	int curlencomTabl = 0;

	std::cout << "������� ������� �������� �� � ��������� ���������"
		                                                  << std::endl;
	bool endin = false;
	while (endin == false)
	{
		std::string strin1;
		std::getline(std::cin, strin1);
		std::string strin = rtrim(ltrim(strin1));

		if (strin == "h" || strin == "help")
		{
			string sss = GetStrHelp(forhelp);
			std::cout << sss << std::endl;
			mesconsol("", 1);
			continue;
		}

		if ((strin == "q") || (strin == "quit") || (strin == "exit"))
			endin = true;
        else
        {
			// ������� ������� ������-�������
			string ames, acommand,aname, afilename = " ";
			int numexistname = -1; 
			int pr = 1;
			Magick::RectangleInfo arect = *(new Magick::RectangleInfo());
			int ret = Parsing(dop, strin, comTabl,
				ames, acommand, aname, afilename,
				numexistname,  pr, arect);

			if (ret == 0)
				mesconsol(ames, 1);
			else
			{
				// ���������� ������� �������
				if (acommand == "load" || acommand == "ld")
				{
					int ret = 0;
					Image tmp = ReadIm(dop, afilename, ret);
					if (ret )
					{
						comElement el =
							comElement(acommand, " ", " ", " ", " ", tmp);
						el.name = aname;
						comTabl.push_back(el);
						markAsDone(strin);
					}
					else
						mesconsol("������ ����������", 1);
				}

				if (acommand == "store" || acommand == "s")
				{
					int ret = 
						WriteIm(dop, afilename, comTabl[numexistname].im);
					if (ret)
					{
						comTabl[numexistname].par2 = afilename;
						markAsDone(strin);
					}
					else
						mesconsol("������ ����������", 1);
				}

				if (acommand == "blur")
				{
					Image im = comTabl[numexistname].im;
					Image imout = *(new Image());

					int ret = BlurIm(aname, im, imout, arect);
					if (ret)
					{

						comElement el =
							comElement(acommand, " ", " ", " ", " ", imout);

						el.name = aname;
						comTabl.push_back(el);

						markAsDone(strin);
					}
					else
						mesconsol("������ ����������", 1);
				}

				if (acommand == "resize")
				{
					Image im = comTabl[numexistname].im;
					Image imout = *(new Image());

					int ret = ResizeIm(aname, im, imout, arect);

					if (ret)
					{
						comElement el =
							comElement(acommand, " ", " ", " ", " ", imout);

						el.name = aname;
						comTabl.push_back(el);

						markAsDone(strin);
					}
					else
						mesconsol("������ ����������", 1);
				}
			}
	    }
	}

	//system("pause");
	return;
}

#pragma region mesconsol
void mesconsol(string s, int pr)
{
	std::cout << s << std::endl;
	if(pr)
		std::cout << "������� ������� �������� �� � ��������� ���������" << std::endl;
	//exit(3);
}
#pragma endregion 

#pragma region 2 �-� ��� ������� ����������� ���������� �������

COORD GetConsoleCursorPosition(HANDLE hConsoleOutput)
{
	CONSOLE_SCREEN_BUFFER_INFO cbsi;
	if (GetConsoleScreenBufferInfo(hConsoleOutput, &cbsi)) 
		return cbsi.dwCursorPosition;
	COORD invalid = { 0, 0 };
	return invalid;
}


void markAsDone(string str)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos = GetConsoleCursorPosition(hConsole);
	COORD posnew = { 0,0 };
	posnew.Y = pos.Y - 1;
	posnew.X += str.length() + 2;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), posnew);
	puts("+++");
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
	return;
}
#pragma endregion

#pragma region GetStrHelp
string GetStrHelp(string& forhelp) {
		streampos size;
		char* memblock;

		ifstream file(forhelp + "help.txt",
			ios::in | ios::binary | ios::ate);

		if (!file.is_open()) return "";

		size = file.tellg();
		memblock = new char[size];
		file.seekg(0, ios::beg);
		file.read(memblock, size);
		string sss = static_cast<string>(memblock);
		sss = sss.substr(0, sss.length() - 25);
		file.close();
		//cout << sss;
		delete[] memblock;
		return sss;
}
#pragma endregion 

#pragma region rtrim
 std::string& rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(),
		std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}
#pragma endregion

#pragma region ltrim
 std::string& ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(),
		std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}
#pragma endregion