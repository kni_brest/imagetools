#pragma once
#include "Header.h"

//������� ��� ������ � ImageMagick 

int Init()
{
	InitializeMagick("");
	return 1;
}

Image ReadIm(string dop, string name, int &ret)
{
	Image img = *(new Image());
	ret = 1;
	try {
		img.read(dop + name);
	}
	catch (...)
	{
		ret = 0;
		img = NULL;
	}
	return img;
	
}

int WriteIm(string dop, string name, Image& im)
{
	int ret = 1;
	try {
		im.write(dop + name);
	}
	catch (...)
	{
		ret = 0;
	}
	return ret; 
}

int BlurIm(string nameout, Image& imin, Image& imout, RectangleInfo rect)
{
	int ret = 1;
	try {
		Image im1 = *(new Image(imin, Geometry(rect)));
		im1.adaptiveBlur(8.0, 5.0);
		imin.composite(im1, rect.x, rect.y);
	}
	catch (...)
	{
		ret = 0;
	}

	imout = imin;

	return ret;
}

int ResizeIm(string nameout, Image& imin, Image& imout, RectangleInfo rect)
{
	int ret = 1;
	try {
		imin.resize( to_string(rect.width) + "x" + to_string(rect.height));
	}
	catch (...)
	{
		ret = 0;
	}

	imout = imin;

	return ret;
}

