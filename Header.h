#pragma once
#include <Magick++.h>
#include<Windows.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <direct.h>


using namespace std;
using namespace Magick;

#pragma region struct comElement
struct comElement
{
public:
	string comd = "";
	string par1 = "";
	string par2 = "";
	string par3 = "";
	string par4 = "";
	string remark = "";
	Image im = (Image)NULL;
	string name = "";

	comElement()
	{
		comd = "";
		par1 = "";
		par2 = "";
		par3 = "";
		par4 = "";
		remark = "";
		im = (Image)NULL;
		name = "";
	}

	comElement
	(string comd, string par1, string par2, string par3, string remark, Image im) {
		
		this->comd = comd;
		this->par1 = par1;
		this->par2 = par2;
		this->par3 = par3;
		this->par4 = par4;
		this->remark = remark;
		this->im = im;
		
	}
};
#pragma endregion 

//�������
int Init();
Image ReadIm(string dop, string name, int& ret);
int WriteIm(string dop, string name, Image& im);
int BlurIm(string nameout, Image& imin, Image& imout, RectangleInfo rect);
int ResizeIm(string nameout, Image& imin, Image& imout, RectangleInfo rect);

std::string& rtrim(std::string& s);
inline std::string& ltrim(std::string& s);

vector<string> split(string data, string token);
bool IsDigit(string s);
bool existFile(string str);
int findName(const std::vector <comElement>& pv, string findname, int load);

int Parsing(string& dop, string& pstr, vector<comElement>& pcomTabl,
	string& rmes, string& rcommand, string& rname, string& rfilename,
	int& numexistname, int pr, Magick::RectangleInfo &rrect);
int ParsingLoad(string& dop, vector<string>& lex, vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename);
int ParsingStore(string& dop, vector<string>& lex, vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int& numexistname, int pr);
int ParsingBlur(string& dop, vector<string>& lex, vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int& numexistname,
	Magick::RectangleInfo& rrect);
int ParsingResize(string& dop, vector<string>& lex, vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int& numexistname,
	Magick::RectangleInfo& rrect);

