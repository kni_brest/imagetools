#pragma once
#include "Header.h"

int Parsing(string &dop, string &pstr, vector<comElement> &pcomTabl, 
	string &rmes, string &rcommand, string &rname, string & rfilename,
	int& numexistname, int pr, Magick::RectangleInfo& rrect)
{
	rmes, rcommand, rname, rfilename = " ";

	vector<string> lex = split(pstr, " ");
	rcommand = lex[0];

	if (rcommand != "load" & rcommand != "store" & rcommand != "blur" &
		rcommand != "resize" & 	rcommand != "ld" & rcommand != "s")
	{
		rmes = "��� ����� �������";
		return 0;
	}

	if (rcommand == "load" || rcommand == "ld")
	  return ParsingLoad(dop, lex, pcomTabl, rmes, rname, rfilename);

	if (rcommand == "store" || rcommand == "s")
	  return ParsingStore(dop, lex, pcomTabl, rmes, rname, rfilename,
		                                                numexistname,pr);

	if (rcommand == "blur")
	  return ParsingBlur(dop, lex, pcomTabl, rmes, rname, rfilename,
		                                            numexistname,  rrect);

	if (rcommand == "resize")
		return ParsingResize(dop, lex, pcomTabl, rmes, rname, rfilename,
			                                         numexistname, rrect);
}

#pragma region  ParsingResize
//resize - �������
//<from_name> -��� �����������
//<to_name> -��� �����������
//<new_width>
//<new_height>
int ParsingResize(string& dop, vector<string>& lex,
	vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int& numexistname,
	Magick::RectangleInfo& rrect)
{
	if (lex.size() != 4)
	{
		rmes = "� ������� blur ������ ���� 4 ���������";
		return 0;
	}

	int indfin = findName(pcomTabl, lex[1], 0);
	if (indfin == -1)
	{
		rmes = "���������� ��� " + lex[1] + " �� ����������";
		return 0;
	}

	numexistname = indfin; //������ ������� �����

	if (findName(pcomTabl, lex[2], 0) != -1)
	{
		rmes = "���������� ��� " + lex[2] + " ��� ����������";
		return 0;
	}

	rname = lex[2]; //����� ���

	vector<string> param = split(lex[3], ",");

	if (param.size() != 2)
	{
		rmes = "����� ������� ����������� ������ ���������� ������ � ������";
		return 0;
	}

	// ����� �����
	int isdig = 1;
	for (int i = 0; i < 2; i++)
	{
		if (!IsDigit(param[i]))
		{
			isdig = 0;
			break;
		}
	}

	if (isdig == 0)
	{
		rmes = "�������� ����� �������� �����������  �������� �� ������ �����";
		return 0;
	}

	RectangleInfo rect = *(new Magick::RectangleInfo());
	int pr = 1;
	try {
		rect.x = 0;
		rect.y = 0;
		rect.width = std::stoi(param[0]);
		rect.height = std::stoi(param[1]);
	}
	catch (...) {
		pr = 0;
	}

	if (pr == 0)
	{
		rmes = "������ � ����������� ����� �������� �����������";
		return 0;
	}

	if (rect.width <= 0 || rect.height <= 0 )
		{
			rmes = "����� ������� ����������� ������ ���� > 0";
			return 0;
		}

	rrect = rect;
	return 1;
}
#pragma endregion

#pragma region  ParsingBlur
//blur - �������
//<from_name> -��� �����������
//<to_name> -��� �����������
//<size> -������ ������������ �������

int ParsingBlur(string& dop, vector<string>& lex, vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int& numexistname,
	Magick::RectangleInfo &rrect)
{
	if (lex.size() != 4)
	{
		rmes = "� ������� blur ������ ���� 3 ���������";
		return 0;
	}

	int indfin = findName(pcomTabl, lex[1], 0);
	if (indfin == -1)
	{
		rmes = "���������� ��� " + lex[1] + " �� ����������";
		return 0;
	}

	numexistname = indfin; //������ ������� �����

	if (findName(pcomTabl, lex[2], 0) != -1)
	{
		rmes = "���������� ��� " + lex[2] + " ��� ����������";
		return 0;
	}

	rname = lex[2]; //����� ���

	vector<string> param = split(lex[3], ",");

	if (param.size() != 4)
	{
		rmes =
			"������� ����������� ������ ��������� ����������� ��������������";
		return 0;
	}

	// ����� �����
	int isdig = 1;
	for (int i = 0; i < 4; i++)
	{
		if (!IsDigit(param[i]))
		{
			isdig = 0;
			break;
		}
	}

	if (isdig == 0)
	{
		rmes = "����������� �������������� ����������� �������� �� �����";
		return 0;
	}

	RectangleInfo rect = *(new Magick::RectangleInfo());
	int pr = 1;
	try {
		rect.x = std::stoi(param[0]);
		rect.y = std::stoi(param[1]);
		rect.width = std::stoi(param[2]);
		rect.height = std::stoi(param[3]);
	}
	catch (...) { 
		pr = 0;
	}
	if (pr == 0)
	{
		rmes = "������ � ����������� �������������� �����������";
		return 0;
	}

	if (  rect.x < 0 || rect.y < 0 
			  || rect.x + rect.width >= pcomTabl[numexistname].im.columns()
			  || rect.y + rect.height >= pcomTabl[numexistname].im.rows()
		)
		if (pr == 0)
		{
			rmes = "������ � �������� �������������� �����������";
			return 0;
		}

	rrect = rect;
	return 1;
}
//

#pragma endregion

#pragma region  ParsingStore
//store - �������
//<name> -��� �����������
//<filename> -��� ����� ��� �����������
int ParsingStore(string& dop, vector<string>& lex,
	vector<comElement>& pcomTabl,
	string& rmes, string& rname, string& rfilename, int &numexistname, int pr)
{
	if (lex.size() != 3)
	{
		rmes = "� ������� store ������ ���� 2 ���������";
		return 0;
	}

	string str = dop + lex[2];
	bool uuu = existFile(str);
	if (existFile(str))
		if (pr == 0)
		{
			rmes = "���� " + str + " ��� ����������";
			return 0;
		}

	rfilename = lex[2];
	int indname = findName(pcomTabl, lex[1], 0);
	if (indname == -1)
	{
		rmes = "���������� ��� " + lex[1] + " �� ����������";
		return 0;
	}
	rname = lex[1];
	numexistname = indname;
	return 1;
}
#pragma endregion

#pragma region  ParsingLoad
//load - �������
//<name> -��� �����������, �� ����� ��� �������� � ������ ��������
//<filename> -��� ����� ��� ��������
int ParsingLoad(string &dop, vector<string> &lex, vector<comElement> &pcomTabl,
	string &rmes, string &rname, string &rfilename )
{
	if (lex.size() != 3)
	{
		rmes =  "� ������� lood ������ ���� 2 ���������";
		return 0;
	}

	string str = dop + lex[2];
	bool uuu = existFile(str);
	if (!existFile(str))
	{
		rmes = "���� �� ����������";
		return 0;
	}
	rfilename = lex[2];

	if (findName(pcomTabl, lex[1], 0) != -1)
	{
		rmes = "������������ �����";
		return 0;
	}
	rname = lex[1];

	return 1;
}
#pragma endregion

#pragma region split_for_string

vector<string> split(string data, string token)
{
	vector<string> output;
	size_t pos = string::npos; // size_t to avoid improbable overflow
	do
	{
		pos = data.find(token);
		output.push_back(data.substr(0, pos));
		if (string::npos != pos)
			data = data.substr(pos + token.size());
	} while (string::npos != pos);
	return output;
}
#pragma endregion

#pragma region existFile
bool existFile(string str)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;

	std::wstring wstr1(str.begin(), str.end());
	std::wstring wstr2(str.length(), ' ');
	std::copy(str.begin(), str.end(), wstr2.begin());
	std::string str1(wstr1.begin(), wstr1.end());
	std::string str2(wstr2.length(), L' ');
	std::copy(wstr2.begin(), wstr2.end(), str2.begin());
	wstring my_str = wstr2;
	LPCWSTR wide_string;

	wide_string = my_str.c_str();
	hf = FindFirstFile(wide_string, &FindFileData);

	FindClose(hf);
	bool ret = false;
	if (hf != INVALID_HANDLE_VALUE)
	{
		FindClose(hf);
		ret = true;
	}
	else
	{
		FindClose(hf);
		ret = false;
	}

	FindClose(hf);
	return ret;
}
#pragma endregion

#pragma region findName
int findName(const std::vector <comElement>& pv, string findname, int load)
{
	int len = pv.size();
	for (int i = 0; i < len; i++)
		if (pv[i].name == findname)
		{
			if (load)
				if (pv[i].comd == "load")
					return i;
				else
				{
				}
			else
				return i;
		}
	return -1;
}
#pragma endregion

#pragma region IsDig
bool IsDigit(string s)
{
	return (count_if(s.begin(), s.end(), isdigit) ==
		s.size()) ? (true) : (false);
}
#pragma endregion